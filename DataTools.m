(* Mathematica Package *)
(* Created by the Wolfram Language Plugin for IntelliJ, see http://wlplugin.halirutan.de/ *)

(* :Title: DataTools *)
(* :Context: DataTools` *)
(* :Author: Alec *)
(* :Date: 2020-09-19 *)

(* :Package Version: 0.2 *)
(* :Mathematica Version: 12.1 *)
(* :Copyright: (c) 2020 Alec *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["DataTools`"];

GetSample::usage = "GetSample[company, startDate, endDate] returns a list of time series over the requested dates";

SeriesListToNumericData::usage = "SeriesListToArray[{list of time series}] returns an un-normalized array of data";

SplitForTrainingNormalized::usage = "Splits input data into sliding windows of length <trainingLength>
    with output labels of average returns in the next 3-6 days. Input is normalized (converted to values 0-1)";

NormalizeInput::usage = "Normalizes all columns in NumericArray data in an intelligent way";

GetFold::usage = "Returns a subset of the original data";

PercentGain::usage = "Computes the change in percent from initial to final";

Begin["`Private`"];

(* Extract the values from time series data *)
timeSeriesExtractValues[series_] := series[[2]][[1]];
timeSeriesExtractValuesMagnitude[series_] := QuantityMagnitude[ timeSeriesExtractValues[series] ][[1]];
godCheckForMissingDataOfAnyKindFuckMe[x_] := With[{values = Map[timeSeriesExtractValues, x]},
  Length[values] < 1
      || MemberQ[MissingQ /@ Flatten[values], True]
];


(* Extract the values from time series data *)
anyDataMissing[datas_] := MemberQ[Map[MissingQ[#] || Length[#] < 2 &, datas], True];

(* Augment the training data with additional metrics *)
PercentGain[open_, close_] := (close - open) / open;
addPercentDailyIncrease[values_] :=  With[{valuesT = Transpose[values]}, (* Transpose features to first dimension *)
  Transpose[
    (* Add %gain metric to feature (open to close % fluctuation) *)
    Append[valuesT, PercentGain[valuesT[[1, All]], valuesT[[4, All]]]]
  ]];

SeriesListToNumericData[financialData_] := If[
  godCheckForMissingDataOfAnyKindFuckMe[financialData], (* if any of the data is MISSING FUCK ME why is this so hard *)
  Print["stonk fail EXTRABAD"]; Nothing,
  With[{values = Map[timeSeriesExtractValuesMagnitude, financialData][[;;-2]] (* Remove the OHLCV *)},
    If[(* Check that we have all the same length of data for all of our fields. *)
      Length[Dimensions[values]] == 2
          (* Note: this check will break if you try to use a single day: *)
          && ! anyDataMissing[values],
      Print["stonk success"]; addPercentDailyIncrease[Transpose[values]],
      Print["stonk fail"]; Nothing]]];



GetSample[company_, start_, end_] := With[
  {data = FinancialData[
    company,
    {"AdjustedOpen",
      "AdjustedHigh",
      "AdjustedLow",
      "AdjustedClose",
      "Average50Day",
      "Average200Day",
      "Volume",
      "Volatility20Day",
      "Volatility50Day",
      "Volatility250Day",
      "AdjustedOHLCV"
    },
    {start, end}]
  },
  If[MissingQ[data] || anyDataMissing[data] || godCheckForMissingDataOfAnyKindFuckMe[data],
    Print["Download fail, ", company, " ", ToString[$KernelID]]; Nothing,
    Print["Download success, ", company, " ", ToString[$KernelID]]; data]];

GetFold[data_, foldNumber_, nFolds_] := With[{foldLength = Floor[Length[data] / nFolds]},
  data[[ (foldNumber-1)  * foldLength + 1 ;; foldNumber * foldLength]]
];

getIOChunk[data_, start_, length_] := With[
  {
    futureClose = N[Mean[data[[start + length + 2 ;; start + length + 6, 4]]]] (* Average closing price 3-7 days out *)
  },
  data[[start ;; start + length - 1]](* slice of training data *) -> futureClose];


NormalizeInput[input_] := With[
  {(*normalize money to maxPrice *)
    maxPrice = GetMaxPrice[input],
    maxVolume = N[Max[Max[input[[All, 7]]], 1]],
    transposed = Transpose[N[input]],
    dataColumns = Dimensions[input][[2]],
    dataLength = Length[input]},
  ArrayReshape[Transpose[NumericArray[Join[
    transposed[[1 ;; 6, All]] / maxPrice, (* Prices and avg normalized to max *)
    {transposed[[7, All]] / maxVolume}, (* Volume normalized to max *)
    transposed[[8 ;; 10, All]] / maxPrice ^ 2, (* Volatility is variance in units of price *)
    {transposed[[11, All]]}
  ], "Real32"]], {1, dataLength, dataColumns}]
];

GetMaxPrice[input_] := N[ Max[ input[[All, 1 ;; 6]], {0.001} ] ];

normalizeSample[sample_] := With[
  {
    input = Keys[sample],
    output = Values[sample]
  },
  NormalizeInput[input] -> output / GetMaxPrice[input]
];


SplitForTrainingNormalized[data_, trainingLength_] :=
    Map[normalizeSample,
      Map[getIOChunk[data, #, trainingLength] &, Range[Length[data] - trainingLength - 6]]
    ];

End[]; (* `Private` *)

EndPackage[]